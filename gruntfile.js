module.exports = function(grunt) {
  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-compass');
  grunt.loadNpmTasks('grunt-contrib-cssmin');
  grunt.loadNpmTasks('grunt-contrib-imagemin');
  grunt.loadNpmTasks('grunt-newer');

  grunt.initConfig({

    /* CONCAT */
    concat: {
      my_target: {
        files: {
          'themes/base-theme/js/dev/common.responsive.js': [
            'vendor/components/jquery/jquery.js',
            'vendor/werkbot/rotate/*.js',
            'vendor/werkbot/sidenav/*.js',
            'vendor/werkbot/slider/*.js',
            'vendor/werkbot/tabs/*.js',
            'themes/base-theme/js/dev/pages/*.js',
          ],
          'themes/base-theme/js/dev/common.js': [
            'vendor/components/jquery/jquery.js',
            'vendor/werkbot/rotate/*.js',
            'vendor/werkbot/slider/*.js',
            'vendor/werkbot/tabs/*.js',
            'themes/base-theme/js/dev/pages/*.js',
          ]
        }
      }
    },

    /*
			UGLIFY
			https://github.com/gruntjs/grunt-contrib-uglify
			Define javascript files here
		*/
    uglify: {
      my_target: {
        files: {
          'themes/base-theme/js/prod/common.responsive.js': [
            'themes/base-theme/js/dev/common.responsive.js'
          ],
          'themes/base-theme/js/prod/common.js': [
            'themes/base-theme/js/dev/common.js'
          ]
        }
      }
    },

    /*
			COMPASS
			https://github.com/gruntjs/grunt-contrib-compass
		*/
    compass: {
      dev: {
        options: {
          config: 'config.rb',
          relativeAssets: true
        }
      }
    },

    /*
			CSSMIN
			https://github.com/gruntjs/grunt-contrib-cssmin
		*/
    cssmin: {
      build: {
        files: {
          'themes/base-theme/css/prod/site.responsive.css': [
            'themes/base-theme/css/dev/site.responsive.css'
          ],
          'themes/base-theme/css/prod/site.noresponsive.css': [
            'themes/base-theme/css/dev/site.noresponsive.css'
          ],
          'themes/base-theme/css/editor.css': [
            'themes/base-theme/css/dev/editor.css'
          ]
        }
      }
    },

    /*
			IMAGEMIN
			https://github.com/gruntjs/grunt-contrib-imagemin
		*/
    imagemin: {
      theme: {
        files: [{
          expand: true,
          cwd: 'themes/base-theme/images/',
          src: ['*.{png,jpg,gif}'],
          dest: 'themes/base-theme/images/'
        }]
      },
      assets: {
        files: [{
          expand: true,
          cwd: 'assets/',
          src: [
            '*.{png,jpg,gif}',
            '**/*.{png,jpg,gif}'
          ],
          dest: 'assets/'
        }]
      }
    },

    /*
			WATCH
			https://github.com/gruntjs/grunt-contrib-watch
		*/
    watch: {
      scripts: {
        files: [
          'themes/base-theme/js/dev/**/*.js'
        ],
        tasks: ['concat', 'uglify']
      },
      sass: {
        files: [
          'themes/base-theme/sass/**/*.scss'
        ],
        tasks: ['compass:dev', 'cssmin:build']
      }
    }

  })
  grunt.registerTask('default', [
    'watch'
  ]);
  grunt.registerTask('build', [
    'concat',
    'uglify',
    'compass:dev',
    'cssmin:build',
    'imagemin:theme',
    'imagemin:assets'
  ]);
}
