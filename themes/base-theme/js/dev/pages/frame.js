/*
  FRAME.JS
  This should contain any javascript that will be executed on every page
*/
(function($) {
	$(window).load(function () {
	});
})(jQuery);
(function($) {
	$(document).ready(function(){
    // SIDENAV
		if(jQuery().sidenav) {
      $('#desktop_nav').sidenav({
        "OpenCloseTime": 500,
        "NavWidth": 80,
        "TriggerWidth": 1025,
        "EasingOpen": "easeOutQuint",
        "EasingClose": "easeOutQuint",
        "Position": "right",
        "SiteContainer": "#sidenav_site_container",
        "SidenavButtonContainer": "#sidenav_button"
      });
      $('#sidenav_button').on('click', function(){
        if($(this).hasClass('open')){
          $('#sidenav_button .open').hide();
          $('#sidenav_button .close').show();
        }else{
          $('#sidenav_button .close').hide();
          $('#sidenav_button .open').show();
        }
      });
    }
	});
})(jQuery);
