<?php
use SilverStripe\CMS\Model\RedirectorPage;
use SilverStripe\CMS\Model\VirtualPage;
//
RedirectorPage::add_extension('RedirectorPageExtension');
VirtualPage::add_extension('VirtualPageExtension');
