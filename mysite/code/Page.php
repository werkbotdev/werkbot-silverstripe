<?php
/**/
use SilverStripe\CMS\Model\SiteTree;
use SilverStripe\Control\ContentNegotiator;
use SilverStripe\Control\Director;
use SilverStripe\Core\Config\Config;
use SilverStripe\Forms\FormField;
use SilverStripe\Forms\OptionsetField;
use SilverStripe\Forms\Tab;
use SilverStripe\Forms\TextField;
use SilverStripe\Forms\TextAreaField;
use SilverStripe\Security\Permission;
/**/
class Page extends SiteTree {
	/**/
	private static $db = [
		'PageLayout' => "Enum('full,sidebar', 'full')",
		'PageTitle' => "Enum('hide,show', 'show')"
  ];
	/*
		SETTINGS
	*/
	public function getSettingsFields() {

		$fields = parent::getSettingsFields();

		return $fields;
	}
	/*
		GET CMS FIELDS
	*/
	public function getCMSFields(){

		$fields = parent::getCMSFields();

    //PAGE LAYOUT
		$fields->addFieldToTab(
			"Root.Main",
			OptionsetField::create(
				'PageLayout',
				'Page Layout',
				array(
					'full' => 'Full Page',
					'sidebar' => 'Content with sidebar'
				),
				'full'
			)
		);

		//PAGE TITLE
		$fields->addFieldToTab(
			"Root.Main",
			OptionsetField::create(
				'PageTitle',
				'Title Show/Hide',
				array(
					'show' => 'Show Page Title',
					'hide' => 'Hide Page Title'
				),
				'show'
			)
		);

    // If any dependant pages, move tab to the end after SEO
    if ($count = $this->DependentPages()->count()) {
      $DependentTab = $fields->findOrMakeTab('Root.Dependent');
      $c = $DependentTab->FieldList();
      $fields->removeByName('Dependent');
      $fields->insertAfter(new Tab('Dependent', 'Dependent'), 'SEO');
      $fields->addFieldsToTab('Root.Dependent', $c->dataFields());
    }

		return $fields;

	}

}
