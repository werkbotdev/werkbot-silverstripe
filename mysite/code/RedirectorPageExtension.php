<?php
use SilverStripe\Forms\FieldList;
use SilverStripe\ORM\DataExtension;
/*
  Redirector Page Extension
  Primarily used to remove unwanted fields
*/
class RedirectorPageExtension extends DataExtension {
  /*
    Update CMS Field
  */
  public function updateCMSFields(FieldList $fields) {

    //$fields->removeFieldFromTab('Root.Main', 'PageLayout');
    //$fields->removeFieldFromTab('Root.Main', 'PageTitle');
    $fields->removeByName('Content');
    $fields->removeByName('AdvancedContent');

  }
}
