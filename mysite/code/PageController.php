<?php
/**/
use SilverStripe\Control\Controller;
use SilverStripe\CMS\Controllers\ContentController;
use SilverStripe\Control\Director;
use SilverStripe\Control\Session;
use SilverStripe\View\Requirements;
use SilverStripe\View\ThemeResourceLoader;
/**/
class PageController extends ContentController{
	/**/
  private static $allowed_actions = array(
		"mobileSiteOn",
		"mobileSiteOff"
  );
  /**/
  protected function init(){
    parent::init();

    //CHECK TO SEE IF MOBILE STYLES/SCRIPTS SHOULD BE LOADED AS WELL
    $loader = SilverStripe\View\ThemeResourceLoader::inst();
    //ALWAYS BLOCK SILVERSTRIPE JQUERY
    Requirements::block('//code.jquery.com/jquery-1.7.2.min.js');
    //
		if($this->getMobileSiteStatus()=="on"){
			if(Director::get_environment_type()=="dev"){
				//CSS
				Requirements::css($loader->getPath("base-theme")."/css/dev/site.responsive.css");
        //JAVASCRIPT
        Requirements::javascript($loader->getPath("base-theme")."/js/dev/common.responsive.js");
			}else{
				//CSS
				Requirements::css($loader->getPath("base-theme")."/css/prod/site.responsive.css");
        //JAVASCRIPT
        Requirements::javascript($loader->getPath("base-theme")."/js/prod/common.responsive.js");
			}
		}else{
			if(Director::get_environment_type()=="dev"){
				//CSS
				Requirements::css($loader->getPath("base-theme")."/css/dev/site.noresponsive.css");
        //JAVASCRIPT
        Requirements::javascript($loader->getPath("base-theme")."/js/dev/common.js");
			}else{
				//CSS
				Requirements::css($loader->getPath("base-theme")."/css/prod/site.noresponsive.css");
        //JAVASCRIPT
        Requirements::javascript($loader->getPath("base-theme")."/js/prod/common.js");
			}
		}
  }
  /*
		GET MOBILE STATUS
		Returns the session cookie that holds whether mobile should be loaded or not. Defaults to "ON"
	*/
	public function getMobileSiteStatus(){
    $session = Controller::curr()->getRequest()->getSession();
		if(!$session->get('MobileSiteOnOff')){
			return "on";
		}else{
			return $session->get('MobileSiteOnOff');
		}
	}
	/*
		MOBILE SITE ON
		Sets the mobile cookie to on
	*/
	public function mobileSiteOn(){
    $session = Controller::curr()->getRequest()->getSession();
    $session->set('MobileSiteOnOff', "on");
		$this->redirectBack();
	}
	/*
		MOBILE SITE OFF
		Sets the mobile cookie to off
	*/
	public function mobileSiteOff(){
    $session = Controller::curr()->getRequest()->getSession();
    $session->set('MobileSiteOnOff', "off");
		$this->redirectBack();
	}
}
