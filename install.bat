call npm install
call npm install -g grunt-cli
call gem install compass
call compass create --bare --sass-dir "themes\base-theme\sass" --css-dir "themes\base-theme\css\dev" --javascripts-dir "themes\base-theme\js\dev" --images-dir "themes\base-theme\images" --fonts-dir "themes\base-theme\webfonts"
call CD httpdocs
call composer install
@echo off
echo Press any key to exit
pause > nul
cls
exit
